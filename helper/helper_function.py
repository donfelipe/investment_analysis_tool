from flask import session, render_template
from werkzeug.datastructures import MultiDict
from wtforms import SelectField

from assumptions.DealTerms import DealTerms, DealTermsFromDict
from assumptions.MarketScenario import MarketScenario, MarketScenarioFromDict
from forms.DynamicDropDown import SelectTerms, SelectMarketScenario
from forms.NewDealTermForm import DealTermForm
from forms.NewMarketScenarioForm import MarketScenarioForm


def check_if_session_has_data(key: str) -> list:
    terms_session = []
    if key in session:
        terms_session = session[key]

    return terms_session


def render_page_complete(template):
    terms_form, new_market_scenario_form = prepare_forms_get_request_existing_forms()
    existing_terms, existing_market_scenarios = prepare_dropdowns()
    return render_template(template, dealTermForm=terms_form, new_market_scenario=new_market_scenario_form,
                           existing_terms=existing_terms(), existing_market_scenarios=existing_market_scenarios())


def default_deal_terms() -> list:
    terms_list = []
    terms = DealTerms(name='Standard Terms', shares_collateral=32500,
                      financing_amount=75000, stock_fee=0.11, pik=0.07, hurdle=0.15,
                      carried_interest=0.1)

    terms_list.append(terms)

    return terms_list


def default_market_scenarios() -> list:
    market_scenario_list = []

    market_scenario = MarketScenario(name='Bad Case', month_to_exit=24, share_price_exit=7.17, share_price_current=7.5,
                                     share_price_current_pref=10.0)

    market_scenario_list.append(market_scenario)

    return market_scenario_list


def get_existing_choices_deal_terms(return_class_object=False) -> list:
    if not return_class_object:
        list_terms = [(0, 'Select')]
        default_terms = default_deal_terms()
        for term in default_terms:
            list_terms.append(tuple([1, term.name]))
        list_saved = check_if_session_has_data('terms')
        for elem in list_saved:
            term = DealTermsFromDict(elem)
            list_terms.append(tuple([len(list_terms), term.name]))
        return list_terms

    else:
        list_terms = default_deal_terms()
        list_saved = check_if_session_has_data('terms')
        for elem in list_saved:
            term = DealTermsFromDict(elem)
            list_terms.append(term)
        return list_terms


def get_existing_choices_market_scenario(return_class_object=False) -> list:
    if not return_class_object:
        list_terms = [(0, 'Select')]
        default_scenarios = default_market_scenarios()
        for scenario in default_scenarios:
            list_terms.append(tuple([1, scenario.name]))

        saved_scenarios = check_if_session_has_data('market')
        for elem in saved_scenarios:
            scenario = MarketScenarioFromDict(elem)
            list_terms.append(tuple([len(list_terms), scenario.name]))

        return list_terms
    else:
        list_scenarios = default_market_scenarios()
        list_saved = check_if_session_has_data('market')
        for elem in list_saved:
            term = MarketScenarioFromDict(elem)
            list_scenarios.append(term)
        return list_scenarios


def prepare_forms_get_request_existing_forms():
    new_terms_form = DealTermForm()
    new_market_scenario_form = MarketScenarioForm()
    return new_terms_form, new_market_scenario_form


def prepare_dropdowns():
    existing_terms = SelectTerms
    existing_market_scenarios = SelectMarketScenario
    setattr(existing_terms, 'Existing Terms',
            SelectField(choices=get_existing_choices_deal_terms(), render_kw={'id': 'drop_terms'}))
    setattr(existing_market_scenarios, 'Existing Market Scenario',
            SelectField(choices=get_existing_choices_market_scenario(), render_kw={'id': 'drop_market'}))

    return existing_terms, existing_market_scenarios


def _handle_post_request_terms(form_data: dict):
    form_data.pop('form_type')
    form = DealTermForm(MultiDict(form_data))
    if form.validate():
        form_data.pop('csrf_token')
        saved_terms = check_if_session_has_data('terms')
        new_elem = DealTermsFromDict(form.data)
        saved_terms.append(new_elem.__dict__)
        session['terms'] = saved_terms

        return render_page_complete('analyze.html')

    else:
        terms_form, new_market_scenario_form = prepare_forms_get_request_existing_forms()
        existing_terms, existing_market_scenarios = prepare_dropdowns()
        return render_template('analyze.html', dealTermForm=terms_form,
                               new_market_scenario=new_market_scenario_form,
                               existing_terms=existing_terms(),
                               existing_market_scenarios=existing_market_scenarios())


def _handle_post_request_market(form_data: dict):
    form_data.pop('form_type')
    form = MarketScenarioForm(MultiDict(form_data))
    if form.validate_on_submit():
        if form.validate():
            form_data.pop('csrf_token')
            saved_market = check_if_session_has_data('market')

            new_elem = MarketScenarioFromDict(form.data)
            saved_market.append(new_elem.__dict__)
            session['market'] = saved_market

            return render_page_complete('analyze.html')


    else:
        terms_form, new_market_scenario_form = prepare_forms_get_request_existing_forms()
        existing_terms, existing_market_scenarios = prepare_dropdowns()
        return render_template('analyze.html', dealTermForm=terms_form,
                               new_market_scenario=new_market_scenario_form,
                               existing_terms=existing_terms(),
                               existing_market_scenarios=existing_market_scenarios())


def handle_post_request(form_data: dict):
    if form_data['form_type'] == 'terms':
        return _handle_post_request_terms(form_data)
    elif (form_data['form_type'] == 'market_scenario'):
        return _handle_post_request_market(form_data)
