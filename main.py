import json

from flask import Flask, render_template, request, session
from werkzeug.datastructures import MultiDict

from assumptions.DealTerms import DealTermsFromDict
from assumptions.MarketScenario import MarketScenarioFromDict
from calculations.ReturnCalculator import ReturnCalculator
from forms.NewDealTermForm import DealTermForm
from forms.NewMarketScenarioForm import MarketScenarioForm
from helper.helper_function import prepare_forms_get_request_existing_forms, prepare_dropdowns, \
    render_page_complete, check_if_session_has_data, get_existing_choices_market_scenario, \
    get_existing_choices_deal_terms, handle_post_request

app = Flask(__name__)
app.config['SECRET_KEY'] = 'aksjfdk;lsajfd;asdfaf234sdadaSD'


@app.route('/analyze', methods=['POST'])
def analyze():
    data = json.loads(request.get_data().decode('utf-8'))
    market_scenario_selected = get_existing_choices_market_scenario(return_class_object=True)[data['index_market'] - 1]

    term_scenario_selected = get_existing_choices_deal_terms(return_class_object=True)[data['index_terms'] - 1]

    calulator = ReturnCalculator(market_scenario_selected,term_scenario_selected)
    result_dict = {'ltv':calulator.calc_ltv(),
                   'stock_component':calulator.calc_stock_component(),
                   'interest_component': calulator.calc_interest_component(),
                   'available_collateral': calulator.calc_available_collateral_component(),
                   'expected_return': calulator.calc_expected_return(),
                   'irr':  calulator.calc_irr_gross(),
                    'irr_net':calulator.calc_irr_net(),
                    'carry':calulator.calc_carry(),
                   'market_scenario': calulator.scenario.name,
                   'terms_scenario': calulator.terms.name
                   }
    return render_template('result.html',info=result_dict)


@app.route('/infoScenario',methods=['POST'])
def info_scenario():
    data = json.loads(request.get_data().decode('utf-8'))
    market_scenario_selected = get_existing_choices_market_scenario(return_class_object=True)[data['index'] - 1]
    dict_data = market_scenario_selected.__dict__
    return render_template('info_scenario.html',info=dict_data)
@app.route('/infoTerms',methods=['POST'])
def info_terms():
    data = json.loads(request.get_data().decode('utf-8'))
    market_scenario_selected = get_existing_choices_deal_terms(return_class_object=True)[data['index'] - 1]
    dict_data = market_scenario_selected.__dict__
    return render_template('info_terms.html',info=dict_data)


@app.route('/', methods=['GET', 'POST'])
def index_route():
    if request.method == 'GET':
        return render_page_complete('analyze.html')
    if request.method == 'POST':
        form_data = request.form.to_dict()
        return handle_post_request(form_data)


if __name__ == '__main__':
    app.run(port=5010)
