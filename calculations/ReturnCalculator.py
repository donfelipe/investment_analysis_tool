import math

from assumptions.DealTerms import DealTerms
from assumptions.MarketScenario import MarketScenario


class ReturnCalculator(object):

    def __init__(self, scenario: MarketScenario, terms: DealTerms) -> None:
        self.scenario = scenario
        self.terms = terms

    def calc_collateral_amount(self) -> float:
        return self.scenario.share_price_current_pref * self.terms.shares_collateral

    def calc_ltv(self) -> float:
        return self.terms.financing_amount / self.calc_collateral_amount()

    def calc_stock_component(self) -> float:

        stock_component = self.terms.stock_fee * self.terms.shares_collateral * self.scenario.share_price_exit

        return stock_component

    def calc_available_collateral_component(self) -> float:
        return self.scenario.share_price_exit * self.terms.shares_collateral

    def calc_interest_component(self) -> float:

        interest_return = math.pow((1 + self.terms.pik), (1 / 12)) - 1

        base = math.pow(1 + interest_return, self.scenario.month_to_exit)

        interest_comp = self.terms.financing_amount * base - self.terms.financing_amount

        return interest_comp

    def calc_expected_return(self):

        components = self.terms.financing_amount + self.calc_stock_component() + self.calc_interest_component()
        collateral = self.calc_available_collateral_component()
        exp_return = min(collateral, components)

        return exp_return

    def calc_irr_gross(self):

        irr_base = self.calc_expected_return() / self.terms.financing_amount
        irr_discount = (1 / (self.scenario.month_to_exit / 12))

        irr_gross = math.pow(irr_base, irr_discount) - 1

        return irr_gross

    def calc_irr_net(self):

        gross_irr = self.calc_irr_gross()

        if gross_irr < self.terms.hurdle:
            return gross_irr
        else:
            return max(self.terms.hurdle, gross_irr - gross_irr * self.terms.carried_interest)

    def calc_carry(self):

        return self.calc_irr_gross() - self.calc_irr_net()
