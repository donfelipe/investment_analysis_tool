from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField ,FloatField, SubmitField

class MarketScenarioForm(FlaskForm):

    name = StringField('Market Scenario Name')
    month_to_exit = IntegerField('Months to Exit')
    share_price_exit = FloatField('Share Price Exit')
    share_price_current = FloatField('Current Share Price')
    share_price_current_pref  = FloatField('Current Share Price (preferred Stock)')

    submit = SubmitField('Add Market Scenario')



