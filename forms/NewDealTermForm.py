from flask_wtf import FlaskForm
from flask_wtf.html5 import IntegerField
from wtforms import FloatField, StringField, SubmitField
from wtforms.validators import DataRequired, NumberRange


class DealTermForm(FlaskForm):

    name = StringField('Deal Term Scenario Name',validators=[DataRequired()])
    shares_collateral = IntegerField('Shares Collateral', validators=[DataRequired(), NumberRange(min=1)])
    financing_amount = FloatField('Financing Amount', [DataRequired(), NumberRange(min=1)], )
    stock_fee = FloatField('Stock Fee', [DataRequired(), NumberRange(min=0)], )
    pik = FloatField('PIK', [DataRequired(), NumberRange(min=0)], )
    hurdle = FloatField('Hurdle', [DataRequired(), NumberRange(min=0)], )
    carried_interest = FloatField('Carried Interest', [DataRequired(), NumberRange(min=0)], )

    submit = SubmitField('Add Terms')
