# Investment Analysis Tool

## Steps

1. Download [Python Version 3.7](https://www.python.org/downloads/)

2. Clone [Respository](git clone https://donfelipe@bitbucket.org/donfelipe/investment_analysis_tool.git)

3. Download virtualen (if not available yet):

    `pip install virtualenv`

3. Create and activate a virtual environment:

      `virtualenv -p python3`
      
      `source bin/activate`
      
      
4. Install requirements:

    `pip install -r 'requirements.txt'`
    
    
5. Start Server
    
    `python main.py`




