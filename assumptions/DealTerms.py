class DealTerms:

    def __init__(self, name: str, shares_collateral: int, financing_amount: float, stock_fee: float,
                 pik: float, hurdle: float,
                 carried_interest: float) -> None:
        self.name = name
        self.shares_collateral = shares_collateral
        self.financing_amount = financing_amount
        self.stock_fee = stock_fee
        self.pik = pik
        self.hurdle = hurdle
        self.carried_interest = carried_interest


class DealTermsFromDict(DealTerms):

    def __init__(self, form_data_dict) -> None:
        for key, val in form_data_dict.items():
            setattr(self, key, val)

        super().__init__(self.name, self.shares_collateral, self.financing_amount, self.stock_fee, self.pik,
                         self.hurdle, self.carried_interest)
