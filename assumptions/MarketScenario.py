class MarketScenario:

    def __init__(self, name: str, month_to_exit: int, share_price_exit: float, share_price_current: float,
                 share_price_current_pref: float, ) -> None:
        self.name = name
        self.month_to_exit = month_to_exit
        self.share_price_exit = share_price_exit
        self.share_price_current = share_price_current
        self.share_price_current_pref = share_price_current_pref


class MarketScenarioFromDict(MarketScenario):
    def __init__(self, form_data_dict) -> None:
        for key, val in form_data_dict.items():
            setattr(self, key, val)

        super().__init__(self.name, self.month_to_exit, self.share_price_exit, self.share_price_current,
                         self.share_price_current_pref)
